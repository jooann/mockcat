/**
 * Mock Logger
 * @author Wangtd
 */
const util = require('util');
const log4js = require('log4js');
const config = require('../config.js');

log4js.configure(config.logConfig);
var logger = log4js.getLogger();

function Logger() {}
Logger.prototype.debug = function(request, message) {
    logger.debug(logMessage.apply(this, arguments));
}
Logger.prototype.info = function(request, message) {
    logger.info(logMessage.apply(this, arguments));
}
Logger.prototype.warn = function(request, message) {
    logger.warn(logMessage.apply(this, arguments));
}
Logger.prototype.error = function(request, message) {
    logger.error(logMessage.apply(this, arguments));
}

function logMessage(request, message) {
    var address = request.headers['x-forwarded-for']
        || request.connection.remoteAddress
        || request.socket.remoteAddress
        || request.connection.socket.remoteAddress;
    var args = [];
    for (var i = 2; i < arguments.length; i++) {
        args.push(arguments[i]);
    }
    // format message
    args.unshift(address);
    args.unshift('[%s] ' + message);
    return util.format.apply(this, args);
}

module.exports = new Logger();
