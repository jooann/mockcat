/**
 * Mock Request Listener
 * @author Wangtd
 */
const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const XML = require('./xml.js');
const JSON5 = require('./json5.js');

function Mocker() {}
Mocker.prototype.mock = function(basedir) {
    return function(request, response, next) {
        var urlObj = url.parse(request.url, true);
        var pathName = urlObj.pathname;
        var isOriginal = urlObj.query && urlObj.query['origin'];

        pathName = decodeURI(pathName);

        var extName = path.extname(pathName);

        if (extName === '') {
            if (pathName.charAt(pathName.length - 1) !== '/') {
                var baseName = path.basename(pathName);
                if (baseName.indexOf('.') === 0) {
                    // when file name is empty
                    extName = baseName;
                } else {
                    // default extension
                    extName = 'json|xml';
                }
            }
        }

        var filePath = null,
            exists = true;

        if (extName === '') {
            // folder
            filePath = path.join(basedir, pathName);
            exists = fs.existsSync(filePath);
            if (exists) {
                try {
                    var indexName = pathName;
                    if (pathName !== '/') {
                        indexName = pathName.substring(0, pathName.lastIndexOf('/'));
                    }

                    var html = '<!DOCTYPE html>';
                    html += '<html lang="zh-CN">';
                    html += '<head>';
                    html += '<meta charset="utf-8">';
                    html += '<link rel="icon" href="/app/imgs/favicon.ico">';
                    html += '<title>Index of ' + indexName + '</title>';
                    // font style
                    html += '<style>body{font-family:"Trebuchet MS",verdana,lucida,arial,helvetica,sans-serif;}</style>';
                    html += '</head>';

                    html += '<body>';
                    html += '<h1>Index of ' + indexName + '</h1>';
                    html += '<table cellspacing="10">';
                    html += '<tr><th align="left">Name</th><th>Last Modified</th><th>Size</th><th>Description</th></tr>';
                    html += '<tr><td><a href="../">Parent Directory</a></td></tr>';

                    // list files
                    var filedir = filePath.substring(0, filePath.lastIndexOf(path.sep));
                    var files = fs.readdirSync(filedir);
                    for (var i in files) {
                        var filename = files[i];
                        var link = filename, size = '';
                        var stat = fs.statSync(path.join(filedir, filename));
                        if(stat.isDirectory()) {
                            link += '/';
                        } else {
                            size = stat.size;
                        }
                        html += '<tr><td><a href="' + link + '">' + link + '</a></td><td>'
                             + moment(stat.mtime).format('ddd MMM DD HH:mm:ss [CST] YYYY') + '</td><td align="right">'
                             + size +'</td></tr>';
                    }

                    html += '</table>';
                    html += '</body>';
                    html += '</html>';

                    response.writeHead(200, { 'content-type': 'text/html' });
                    response.end(html);
                } catch (e) {
                    console.error(e);
                    response.writeHead(500, { 'content-type': 'text/html' });
                    response.end('<h1>500 Server Error</h1><pre>' + e + '</pre>');
                }
            }
        } else {
            // file
            if (extName == 'json|xml') {
                extName = '.json';
                filePath = path.join(basedir, pathName + extName);
                exists = fs.existsSync(filePath);
                if (!exists) {
                    extName = '.xml';
                    filePath = path.join(basedir, pathName + extName);
                    exists = fs.existsSync(filePath);
                }
            } else {
                filePath = path.join(basedir, pathName);
                exists = fs.existsSync(filePath);
            }
            if (exists) {
                try {
                    var status = 200;
                    if (urlObj.query && urlObj.query['status']) {
                        status = parseInt(urlObj.query['status']);
                    }

                    var content = fs.readFileSync(filePath, 'utf-8'),
                        modified = fs.statSync(filePath).mtime.getTime();
                    if (isOriginal || (extName !== '.json' && extName !== '.xml')) {
                        response.writeHead(status, {
                            'content-type': 'text/plain;charset=utf-8',
                            'x-modified': modified
                        });
                    } else if (extName === '.json') {
                        content = JSON.stringify(JSON5.parse(content));
                        response.writeHead(status, {
                            'content-type': 'application/json;charset=utf-8',
                            'x-modified': modified
                        });
                    } else if (extName === '.xml') {
                        content = XML.format(content, { method: 'xmlmin' }).trim();
                        response.writeHead(status, {
                            'content-type': 'application/xml;charset=utf-8',
                            'x-modified': modified
                        });
                    }
                    response.end(content);
                } catch (e) {
                    console.error(e);
                    response.writeHead(500, { 'content-type': 'text/html' });
                    response.end('<h1>500 Server Error</h1><pre>' + e + '</pre>');
                }
            }
        }

        if (!exists) {
            response.writeHead(404, { 'content-type': 'text/html' });
            response.end('<h1>404 Not Found</h1>');
        }
    };
}

module.exports = new Mocker();
