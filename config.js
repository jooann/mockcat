/**
 * Configurations
 * @author Wangtd
 */
const paths = require('./lib/util.js').paths;

module.exports = {
    port: 8181,
    mockDir: paths('../work/mock'),
    blogDir: paths('../work/blog'),
    logConfig: {
        appenders: {
            'console': { type: 'console' },
            'logfile': {
                type: 'file',
                filename: './logs/stdout.log',
                maxLogSize: 10485760,
                backups: 5,
                encoding: 'utf-8'
            }
        },
        categories: {
            'default': {
                appenders: ['console', 'logfile'],
                level: 'info'
            }
        }
    }
};
