# Mockcat 开发工具集

### 项目简介
Mockcat是一款前端与后台分离开发的简易的接口Mock数据工具。提供了Mock数据的模块化管理，接口数据的定义和测试（支持JSON5和XML格式的数据），Mock数据的操作日志，以及数据的Mock服务功能。另外还提供了开发中常用的一些工具及收藏了工具类的网站。


### 安装运行

1.安装[Node.js](https://nodejs.org/en/)

2.在控制台执行以下命令：

  &gt; npm install

  &gt; npm start


### 使用说明

**1.添加Mock数据模块**

点击【添加模块】按钮可添加新的Mock数据模块。对于已添加的数据模块可以修改和删除。

![MockModule](https://images.gitee.com/uploads/images/2019/0109/122449_87b5a132_409346.png "mock-module.png")

**2.Mock数据文件编辑**

点击【MOCK数据模块】页面中的模块名进入到Mock数据的编辑页面。Mock数据的存储使用的文件系统存储的，与URL路径相对应，Mock数据的操作与文件操作相同。选择目录树的结点，可以添加、修改或删除结点，点击【复制】按钮，在列表中可选择不同的操作复制数据、文件或文件夹。点击【导出】按钮，可以选择导出入参和出参的JOPO文件。点击【测试】可测试本地的Mock接口(类似于[Postman](https://www.getpostman.com/)，但支持JSON5和XML的格式与注释)，点击【链接地址】可查看Mock数据或文件列表。

![MockEditor](https://images.gitee.com/uploads/images/2019/0110/120233_5c23d6d7_409346.png "mock-editor.png")

**3.Mock数据编辑器**

Mock数据目录只支持JSON和XML格式的数据，可以在JSON和XML数据中加入注释。一般会添加文件的头注释（对接口进行说明），也可以对字段进行注释（说明字段的含义）。*注：/\*\*/和<!---->注释不能嵌套，注释和数据编辑都可以使用快捷键操作，参照如下的快捷键列表。*

  3.1全屏与查找替换

  ![Editor](https://images.gitee.com/uploads/images/2019/0110/120255_42e9a1eb_409346.png "editor.png")

  3.2支持的快捷键列表

  ![ShortcutKey](https://images.gitee.com/uploads/images/2019/0110/120313_41dcf8fe_409346.png "shortcut-key.png")

**4.Mock接口测试**

存储到系统中的JSON和XML数据文件，可以用来测试本地的API接口。点击【测试】，弹出的测试Mock接口的对话框，会将模板中的URL、Method和入参直接填充到对应的输入框中，然后点击【发送】即可。首次的URL地址将会使用`http://127.0.0.1:8080`作为默认的服务器地址，可以手动修改为实际的API服务器地址，后续将会记录到Cookie中，不需要反复输入。若测试的接口需要登录认证，则需要先登录认证后再测试该接口。

![MockTest](https://images.gitee.com/uploads/images/2019/0110/120331_38f7077c_409346.png "mock-test.png")

**5.数据的Mock服务**

开发过程中前端与后台开发都可协商修改接口数据结构，编辑好的Mock可以用于前端开发页面使用了。选择不同的目录结点，点击【链接地址】可查看Mock数据，如图所示。待后台开发完成后可将服务器地址切换到开发服务器进行调试了，可以在地址中添加`status`参数来指定返回的状态码。*注：编辑器对JSON和XML数据分别按JSON5和XML的标准进行了校验，只有符合标准才能保存和转换成Mock数据。*

![MockDirectory](https://images.gitee.com/uploads/images/2018/0822/165001_366757fc_409346.png "mock-directory.png")

![MockData](https://images.gitee.com/uploads/images/2018/1128/164655_0dcb13b9_409346.png "mock-data.png")

### 其他工具

#### HTTP请求发送
发送HTTP请求，是一款网页版的用于测试HTTP请求的工具。

#### TEXT格式化
格式化JSON/XML/SQL等多种类型数据，并且可以校验一些数据结构的正确性。

#### POJO对象推导
根据JSON/XML/SQL建表语句的文本内容推导出对应结构的JavaBean文件代码。

#### SQL参数匹配
针对MyBatis的SQL日志，将Preparing与Parameters进行匹配和SQL格式化。

#### ApiDoc文档注释
输入对应的注释参数，可生成[apiDoc](http://apidocjs.com/)文档注释。

#### MyBatis映射文件
根据建表SQL语句和DAO/Model/Mapper模板生成对应的MyBatis的映射文件。

#### RegExp正则表达式
摘抄了百度百科关于正则表达式的知识点，可以参考和测试正则表达式。

#### Markdown文件编辑
网页版的Markdown文件编辑器，支持Markdown的基本语法、数学公式、流程图和代码高亮。

#### Underscore.js模板代码
基于Underscore.js模板语法，可以将JSON格式的数据填充到模板中，生成自定义的模板代码。

#### 工具网站收藏
收藏了一些开发中常用的工具网站。

#### 共享博客平台
一个属于自己的小型博客网站，可以记录、分享个人的学习笔记和博客文章。
