define([
    'config',
    'angularAMD',
    'js/init',
    'css!css/default'
], function(config, angularAMD) {
    var app = angular.module('app', ['app.main']);

    app.config(function($routeProvider, $httpProvider, cfpLoadingBarProvider) {
        $routeProvider.otherwise({
            redirectTo: config.defaultPath
        });

        // loading bar's settings
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;

        // http interceptor
        $httpProvider.interceptors.push(function($injector) {
            return {
                responseError: function(rejection) {
                    // error
                    if (rejection.status == -1) {
                        $injector.get('xDialog').alert('错误信息', '服务器已断开连接或连接超时！');
                    } else if (rejection.status >= 400) {
                        if (!/^\/mock\/test\//.test(rejection.config.url)) {
                            $injector.get('xDialog').alert('错误信息', rejection.data);
                        }
                    }
                    return rejection;
                }
            };
        });
    });

    app.run(function($rootScope, $route, $timeout, $location) {
        // override location path
        var original = $location.path;
        $location.path = function(path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function() {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        }

        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
            $rootScope.routeName = current.$$route.name;
        }); 

        // resize page scope
        var callbacks = {},
            resizePageScope = function() {
                var maxHeight = Math.max(jQuery(window).height(), 600) - 180,
                    maxWidth = Math.max(jQuery(window).width(), 1024);
                jQuery('.x-page-scope').css('height', maxHeight + 'px');
                jQuery('.x-page-scope .x-editor pre').css('min-height', maxHeight + 'px');
                // callback
                if (typeof(callbacks[$rootScope.routeName]) === 'function') {
                    callbacks[$rootScope.routeName](maxHeight, maxWidth);
                }
            }
        $rootScope.refresh = function(callback) {
            callbacks[$rootScope.routeName] = callback;
            $timeout(resizePageScope);
        }
        jQuery(window).bind('load resize', resizePageScope);

        // initialize menu
        $rootScope.init = function() {
            jQuery('.navbar-header, .navbar-nav>li').each(function() {
                var $this = jQuery(this);
                $this.removeClass('active');
                var href = $this.find('a').attr('href').substring(2);
                var path = $location.path() == '/' ? config.defaultPath : $location.path();
                if (path.indexOf(href) >= 0) {
                    $this.addClass('active');
                }
                $this.click(function() {
                    jQuery('.navbar-header, .navbar-nav>li').removeClass('active');
                    $this.addClass('active');
                });
            });
        }
    });

    return angularAMD.bootstrap(app);
});
