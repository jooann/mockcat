define([], function() {
    var dataTableDirective = angular.module('app.directive.mapTable', []);

    dataTableDirective.directive('mapTable', function() {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                mtType: '<',
                mtInputs: '=',
                mtCheckbox: '<'
            },
            template: 
                '<table class="table table-bordered table-condensed">' +
                '  <thead>' +
                '    <tr ng-if="mtCheckbox">' +
                '      <th width="10%" class="text-center">' +
                '        <input type="checkbox" ng-change="selectAll(selected)" ng-model="selected">' +
                '      </th>' +
                '      <th width="45%">Key</th>' +
                '      <th width="45%">Value</th>' +
                '    </tr>' +
                '    <tr ng-if="!mtCheckbox">' +
                '      <th width="50%">Key</th>' +
                '      <th width="50%">Value</th>' +
                '    </tr>' +
                '  </thead>' +
                '  <tbody>' +
                '    <tr ng-repeat="input in mtInputs">' +
                '      <td ng-if="mtCheckbox" class="text-center">' +
                '        <input type="checkbox" ng-model="input.checked" ng-change="select()" ng-disabled="input.disabled">' +
                '      </td>' +
                '      <td>' +
                '        <div ng-if="mtType == \'form-data\'" class="input-group input-group-sm">' +
                '          <input type="text" class="form-control" ng-model="input.name" ng-change="check(input)"' +
                '              ng-disabled="input.disabled" placeholder="New key">' +
                '          <span class="input-group-addon">' +
                '            <select ng-model="input.type" ng-disabled="input.disabled">' +
                '              <option value="text" selected>Text</option>' +
                '              <option value="file">File</option>' +
                '            </select>' +
                '          </span>' +
                '        </div>' +
                '        <div ng-if="mtType != \'form-data\'">' +
                '          <input type="text" class="form-control input-sm" ng-model="input.name" ng-change="check(input)"' +
                '              ng-disabled="input.disabled" placeholder="New key">' +
                '        </div>' +
                '      </td>' +
                '      <td>' +
                '        <div ng-if="input.type != \'file\'">' +
                '          <input type="text" class="form-control input-sm" ng-model="input.value"' +
                '              ng-disabled="input.disabled" placeholder="value">' +
                '        </div>' +
                '        <div ng-if="input.type == \'file\'">' +
                '          <input type="file" name="{{input.name}}" ng-disabled="input.disabled">' +
                '        </div>' +
                '      </td>' +
                '    </tr>' +
                '  </tbody>' +
                '</table>',
            link: function(scope, element) {
                if (scope.mtCheckbox == undefined) {
                    scope.mtCheckbox = true;
                }
                scope.selectAll = function(selected) {
                    _.forEach(scope.mtInputs, function(input) {
                        if (!input.disabled) {
                            input.checked = selected;
                        }
                    });
                    syncStatus();
                }

                scope.select = function() {
                    syncStatus();
                }

                scope.check = function(input) {
                    if (!/^[0-9A-Za-z_-]+$/.test(input.name)) {
                        input.name = input.name.replace(/[^0-9A-Za-z_-]/, '');
                    }

                    var index = scope.mtInputs.indexOf(input);
                    if (index == scope.mtInputs.length - 1) {
                        if (input.name != '') {
                            scope.mtInputs.push({
                                name: '',
                                type: 'text',
                                value: '',
                                checked: true,
                                disabled: false
                            });
                            syncStatus();
                        }
                    } else {
                        if (input.name == '') {
                            scope.mtInputs.splice(index, 1);
                            syncStatus();
                        }
                    }
                }

                // initial
                if (_.isEmpty(scope.mtInputs) || _.last(scope.mtInputs).name != '') {
                    scope.mtInputs.push({
                        name: '',
                        type: 'text',
                        value: '',
                        checked: true,
                        disabled: false
                    });
                }
                syncStatus();

                function syncStatus() {
                    var selected = 0, unselected = 0;
                    _.forEach(scope.mtInputs, function(input) {
                        input.checked ? (selected++) : (unselected++);
                    });

                    // clear status of select all
                    element.find('input[type="checkbox"]:eq(0)').prop('indeterminate', '');

                    if (selected == scope.mtInputs.length) {
                        scope.selected = true;
                    } else if (unselected == scope.mtInputs.length) {
                        scope.selected = false;
                    } else {
                        element.find('input[type="checkbox"]:eq(0)').prop('indeterminate', true);
                    }
                }
            }
        }
    });

    return dataTableDirective;
});
