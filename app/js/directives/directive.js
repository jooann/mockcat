define([
    'angular-animate',
    'angular-ui-bootstrap',
    'ng-prettyjson',
    'ng-prettyjson-tmpl',
    'js/directives/markdown',
    'js/directives/map-table',
    'js/directives/api-params'
], function() {
    var directive = angular.module('app.directive', ['ngAnimate', 'ui.bootstrap', 'ng-showdown', 'ngPrettyJson',
        'app.directive.markdown', 'app.directive.mapTable', 'app.directive.apiParams']);

    directive.config(['$uibTooltipProvider', function($uibTooltipProvider) {
        $uibTooltipProvider.options({
            animation: false,
            appendToBody: false,
            placement: 'auto',
            popupCloseDelay: 0,
            popupDelay: 0,
        });
    }]);

    directive.directive('autofocus', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element[0].focus();
            }
        };
    });

    return directive;
});
