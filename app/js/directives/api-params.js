define([
    'js/factories/factory',
], function() {
    var apiParamsDirective = angular.module('app.directive.apiParams', ['app.factory.dialog']);

    apiParamsDirective.directive('apiParams', ['xDialog', function(xDialog) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                params: '=apiParams'
            },
            template:
                '<div class="panel-group">' + 
                '  <div class="panel panel-default" ng-repeat="param in params" ng-init="param.show = false">' +
                '    <div class="panel-heading">' +
                '      <h4 class="panel-title">' +
                '        <input type="checkbox" ng-model="param.checked"> @{{param.name}}' +
                '        <div class="btn-group pull-right">' +
                '          <button type="button" class="btn btn-default btn-xs" ng-click="add(param.rows)" ng-if="param.multiple" title="添加">' +
                '            <i class="glyphicon glyphicon-plus"></i></button>' +
                '          <button type="button" class="btn btn-default btn-xs" ng-click="clear(param)" title="删除">' +
                '            <i class="glyphicon glyphicon-trash"></i></button>' +
                '          <button type="button" class="btn btn-default btn-xs"  ng-click="param.show = !param.show" title="{{param.show ? \'折叠\' : \'展开\'}}">' +
                '            <i class="glyphicon" ng-class="{\'glyphicon-chevron-down\': param.show, \'glyphicon-chevron-right\':!param.show}"></i></button>' +
                '        </div>' +
                '      </h4>' +
                '    </div>' +
                '    <div class="panel-collapse collapse" ng-class="{\'in\': param.show, \'\': !param.show}">' +
                '      <div class="panel-body">' +
                '        <div class="row" ng-show="param.show" ng-repeat="row in param.rows">' +
                '          <div class="hidden" ng-init="row.index = $index + 1"></div>' +
                '          <div class="col-md-{{item.width}} form-group" ng-repeat="item in row" ng-switch="item.type" style="{{row.index == param.rows.length ? \'margin-bottom:0\':\'\'}}">' +
                '            <label class="control-label sr-only">{{item.name}}</label>' +
                '            <select class="form-control" ng-switch-when="select" ng-model="item.value" ng-disabled="!param.checked" ng-options="it.value as it.label for it in item.options"></select>' +
                '            <input type="text" class="form-control" placeholder="{{item.placeholder}}" ng-dblclick="copy(param.rows, row, item)" ng-switch-when="text" ng-model="item.value" ng-disabled="!param.checked">' +
                '            <textarea class="form-control" rows="10" placeholder="{{item.placeholder}}" ng-switch-when="textarea" ng-model="item.value" ng-disabled="!param.checked"></textarea>' +
                '          </div>' +
                '        </div>' +
                '      </div>' +
                '    </div>' +
                '  </div>' +
                '</div>',
            link: function(scope) {
                scope.add = function(rows) {
                    var row = angular.copy(rows[0]);
                    clearRow(row);
                    rows.push(row);
                };
                scope.copy = function(rows, row, item) {
                    var index = _.indexOf(rows, row);
                    if (index > 0) {
                        item.value = rows[index - 1][_.indexOf(row, item)].value;
                    }
                };
                scope.clear = function(param) {
                    xDialog.confirm('确认信息', '您确定要清除【' + param.name + '】的内容吗？').then(function(yes) {
                        if (yes) {
                            _.forEach(param.rows, function(items) {
                                clearRow(items);
                            });
                        }
                    });
                };
                function clearRow(items) {
                    _.forEach(items, function(item) {
                        if (item.type == 'select') {
                            item.value = item.options[0];
                        } else {
                            item.value = '';
                        }
                    });
                }
            }
        };
    }]);

    return apiParamsDirective;
});
