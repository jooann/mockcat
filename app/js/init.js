define([
    'angular-loading-bar',
    'angular-ui-bootstrap',
    'js/controllers/controller',
    'js/directives/directive',
    'js/factories/factory'
], function() {
    return angular.module('app.main', [
        'angular-loading-bar',
        'ui.bootstrap',
        'app.controller',
        'app.directive',
        'app.factory'
    ]);
});
