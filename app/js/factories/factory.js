define([
    'moment',
    'angular-cookies',
    'angular-toastr.tpls',
    'js/factories/util',
    'js/factories/dialog',
    'js/factories/location'
], function(moment) {
    var factory = angular.module('app.factory', ['ngCookies', 'ngAnimate', 'toastr', 'app.factory.util',
        'app.factory.dialog', 'app.factory.location']);

    factory.config(['$cookiesProvider', '$qProvider', 'toastrConfig', function($cookiesProvider, $qProvider,
            toastrConfig) {
        // cookies
        $cookiesProvider.defaults.expires = moment().add(30, 'days').toDate();

        // $q
        $qProvider.errorOnUnhandledRejections(false);

        // angular-toastr config
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-bottom-right',
            preventDuplicates: false,
            preventOpenDuplicates: true,
            target: 'body',
            timeOut: 3000
        });
    }]);

    return factory;
});
