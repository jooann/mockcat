define([
    'angular-loading-bar',
    'ng-file-upload-shim'
], function() {
    var utilFactory = angular.module('app.factory.util', ['angular-loading-bar', 'ngFileUpload']);

    utilFactory.factory('xUtil', ['$q', 'Upload', 'cfpLoadingBar', function($q, Upload, cfpLoadingBar) {
        return {
            reg: {
                subMatch: function(string, regex) {
                    var match = regex.exec(string);
                    if (match != null && match.length >= 2) {
                        return match[1];
                    }
                    return '';
                }
            },
            file: {
                open: function(file) {
                    var defer = $q.defer();

                    // open file
                    Upload.upload({
                        url: '/mock/tool/read',
                        method: 'POST',
                        data: {
                            'Content-Type': (file.type != '' ? file.type : 'application/octet-stream'),
                            'filename': file.name,
                            'file': file
                        }
                    }).then(function(response) {
                        defer.resolve(response.data);
                    }, function(data) {
                        defer.reject(data);
                    });

                    return defer.promise;
                },
                save: function(filename, content) {
                    var jqTargetId = 'jqFormIO-' + (new Date().getTime());

                    var jqForm = jQuery('<form style="display:none">').attr({
                            method: 'POST',
                            action: '/mock/tool/write',
                            target: jqTargetId
                        }).appendTo(document.body);

                    var jqIframe = jQuery('<iframe style="display:none">').attr({
                            name: jqTargetId
                        }).appendTo(document.body);

                    // form data
                    jqForm.append(jQuery('<input type="hidden">').attr('name', 'filename').val(filename));
                    jqForm.append(jQuery('<textarea>').attr('name', 'content').val(content));

                    // submit
                    cfpLoadingBar.start();
                    jqForm.submit();

                    // remove
                    setTimeout(function() {
                        jqForm.remove();
                        jqIframe.remove();
                        cfpLoadingBar.complete();
                    }, 5000);
                }
            },
            image: {
                upload: function(file) {
                    var defer = $q.defer();

                    // upload image
                    Upload.upload({
                        url: '/mock/blog/upload',
                        method: 'POST',
                        data: {
                            'Content-Type': (file.type != '' ? file.type : 'application/octet-stream'),
                            'filename': file.name,
                            'file': file
                        }
                    }).then(function(response) {
                        defer.resolve(response.data);
                    }, function(data) {
                        defer.reject(data);
                    });

                    return defer.promise;
                }
            }
        }
    }]);

    return utilFactory;
});
