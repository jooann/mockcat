define([
    'angularAMD',
    'js/controllers/dialog',
    'angular-sanitize',
    'angular-ui-bootstrap'
], function(angularAMD, dialogs) {
    var dlgFactory = angular.module('app.factory.dialog', ['ngSanitize', 'ui.bootstrap']);

    dlgFactory.factory('xDialog', ['$q', '$uibModal', function($q, $uibModal) {
        var dlgFactory = {};

        var modalInstances = {};
        angular.forEach(dialogs, function(dialogConfigs) {
            angular.forEach(dialogConfigs, function(dialogConfig) {
                modalInstances[dialogConfig.method] = function(context) {
                    var modalInstance = $uibModal.open(angularAMD.route(angular.extend({
                        backdrop: 'static',
                        size: 'md',
                        controllerAs: 'dvm',
                        resolve: {
                            xContext: function () {
                                return context;
                            }
                        }
                    }, dialogConfig)));

                    var defer = $q.defer();
                    modalInstance.result.then(function(data) {
                        defer.resolve(data);
                    }, function(data) {
                        defer.reject(data);
                    });

                    return defer.promise;
                };
            });
        });

        dlgFactory.open = function() {
            return modalInstances;
        }

        dlgFactory.alert = function(title, message) {
            var modalInstance = $uibModal.open({
                backdrop: 'static',
                size: 'md',
                templateUrl: 'templates/dialog/alert.html',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.title = title;
                    $scope.message = message;
                    $scope.ok = function () {
                        $uibModalInstance.close('close');
                    };
                }]
            });

            var defer = $q.defer();
            modalInstance.result.then(function() {
                defer.resolve();
            });

            return defer.promise;
        }

        dlgFactory.confirm = function(title, message) {
            var modalInstance = $uibModal.open({
                backdrop: 'static',
                size: 'md',
                templateUrl: 'templates/dialog/confirm.html',
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.title = title;
                    $scope.message = message;
                    $scope.ok = function () {
                        $uibModalInstance.close('close');
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            });

            var defer = $q.defer();
            modalInstance.result.then(function() {
                defer.resolve(true);
            }, function() {
                defer.resolve(false);
            });

            return defer.promise;
        }

        return dlgFactory;
    }])
    .run(['$templateCache', function($templateCache) {
        $templateCache.put('templates/dialog/alert.html',
                '<div class="modal-header">' +
                '  <button type="button" class="close" ng-click="ok()">&times;</button>' +
                '  <h4 class="modal-title">{{title}}</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '  <p ng-bind-html="message"></p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '  <button type="button" class="btn btn-primary" ng-click="ok()" autofocus>确定</button>' +
                '</div>');
        $templateCache.put('templates/dialog/confirm.html',
                '<div class="modal-header">' +
                '  <button type="button" class="close" ng-click="cancel()">&times;</button>' +
                '  <h4 class="modal-title">{{title}}</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '  <p ng-bind-html="message"></p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '  <button type="button" class="btn btn-primary" ng-click="ok()" autofocus>确定</button>' +
                '  <button type="button" class="btn btn-default" ng-click="cancel()">取消</button>' +
                '</div>');
    }]);

    return dlgFactory;
});
