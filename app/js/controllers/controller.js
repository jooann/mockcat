define([
    'angularAMD',
    'js/controllers/router',
    'angular-route'
], function(angularAMD, routes) {
    var controller = angular.module('app.controller', ['ngRoute']);

    controller.config(['$routeProvider', function($routeProvider) {
        // load routes
        angular.forEach(routes, function(routeConfigs) {
            angular.forEach(routeConfigs, function(routeConfig) {
                routeConfig = angular.extend({
                    controllerAs: 'vm'
                }, routeConfig);
                $routeProvider.when(routeConfig.url, angularAMD.route(routeConfig));
            });
        });
    }]);

    return controller;
});
