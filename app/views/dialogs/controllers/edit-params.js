define(['app'], function(app) {
    app.controller('EditParamsController', function(xContext, $uibModalInstance) {
        var self = this;

        self.init = function() {
            self.params = xContext.params;
        }

        self.close = function() {
            $uibModalInstance.close();
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
