define(['app'], function(app) {
    app.controller('EditModuleController', function($http, xContext, xDialog, $uibModalInstance, toastr) {
        var self = this;
        self.origin = self.module = '';

        var isCreated = false;

        self.init = function() {
            self.origin = self.module = xContext;
            isCreated = (self.origin == '');
        }

        self.save = function(valid) {
            if (valid) {
                if (isCreated) {
                    $http.post('/mock/module/save', { module: self.module }).then(function(response) {
                        if (response.data) {
                            toastr.success('数据模块【' + self.module + '】已添加！');
                            $uibModalInstance.close(true);
                        }
                    });
                } else {
                    $http.post('/mock/module/rename', {
                        oldModule: self.origin,
                        newModule: self.module
                    }).then(function(response) {
                        if (response.data) {
                            toastr.success('数据模块【' + self.origin + '】已修改！');
                            $uibModalInstance.close(true);
                        } else {
                            xDialog.alert('提示信息', '您重命名的模块已存在！');
                        }
                    });
                }
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
