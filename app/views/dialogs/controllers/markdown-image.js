define(['app'], function(app) {
    app.controller('MarkdownImageController', function(xUtil, $uibModalInstance) {
        var self = this;
        self.imageUrl = '';
        self.imageName = '';

        self.init = function() {
            // nothing
        }

        self.upload = function(file) {
            if (file != null) {
                if (/\.(jpg|png|gif)$/i.test(file.name)) {
                    self.imageName = file.name;
                    // upload image
                    xUtil.image.upload(file).then(function(filename) {
                        self.imageUrl = '/app/blog/images/' + filename;
                    });
                } else {
                    xDialog.alert('提示信息', '您上传的不是有效的图片文件！');
                }
            }
        }

        self.insert = function(valid) {
            if (valid) {
                $uibModalInstance.close({ url: self.imageUrl, name: self.imageName });
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
