define(['app'], function(app) {
    app.controller('MarkdownTableController', function($uibModalInstance) {
        var self = this;
        self.rows = 3;
        self.columns = 3;

        self.init = function() {
            // nothing
        }

        self.insert = function(valid) {
            if (valid) {
                $uibModalInstance.close({
                    rows: parseInt(self.rows),
                    columns: parseInt(self.columns)
                });
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
