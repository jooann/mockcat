define(['app', 'text!text/markdown-emoji.json'], function(app, emojis) {
    app.controller('MarkdownEmojiController', function($sce, $showdown, $uibModalInstance) {
        var self = this;
        self.emojis = [];

        self.init = function() {
            self.emojis = JSON.parse(emojis);
        }

        self.toHtml = function(emoji) {
            var title = emoji.replace(/^:|:$/g, '').replace(/_/g, ' ');
            return $sce.trustAsHtml($showdown.makeHtml(emoji)
                    .replace(/<p>(.+)<\/p>/, '<a href="javascript:void(0)" title="' + title + '">$1</a>'));
        }

        self.select = function(emoji) {
            $uibModalInstance.close(emoji);
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
