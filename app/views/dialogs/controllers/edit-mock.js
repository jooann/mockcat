define(['app', 'json2'], function(app, JSON2) {
    app.controller('EditMockController', function($scope, $http, xContext, $uibModalInstance, xDialog, toastr) {
        var self = this;
        self.module = '';
        self.type = 'folder';
        self.parent = '';
        self.folder = '';
        self.file = '';
        self.modifiable = true;
        self.extname = '.json';
        self.oldname = '';
        self.changed = true;

        var nodeName = '';

        self.init = function() {
            self.module = xContext.module;
            self.modifiable = xContext.modifiable;

            // reset form
            nodeName = xContext.treeNode.name;
            if (self.modifiable) {
                if (xContext.treeNode.root) {
                    self.parent = '/';
                } else {
                    self.parent = xContext.treeNode.parent + '/' + nodeName;
                }
            } else {
                var dotIndex = nodeName.lastIndexOf('.');
                if (dotIndex == -1) {
                    self.folder = nodeName;
                } else {
                    var extname = nodeName.substring(dotIndex);
                    if (extname) {
                        self.type = 'file';
                        self.extname = extname;
                        self.file = nodeName.substring(0, nodeName.length - extname.length);
                    }
                }
                self.oldname = self.folder || self.file;
                self.parent = xContext.treeNode.parent;
            }

            $scope.$watchGroup(['dvm.folder', 'dvm.file'], function(newValue) {
                if (self.type == 'folder') {
                    self.changed = (self.oldname != newValue[0]);
                } else {
                    self.changed = (self.oldname != newValue[1]);
                }
            })
        }

        self.save = function(valid) {
            if (valid) {
                if (self.modifiable) {
                    var folder = self.parent;
                    if (self.type == 'folder') {
                        folder = self.parent + '/' + self.folder;
                    }
                    $http.post('/mock/data/save', {
                        created: true,
                        module: self.module,
                        type: self.type,
                        folder: folder,
                        file: self.file + self.extname,
                        data: (self.extname == '.xml' ? '<xml/>' : '{}')
                    }).then(function(response) {
                        if (response.data) {
                            if (self.type == 'file') {
                                toastr.success('文件【' + self.file + self.extname +'】已添加！');
                            } else {
                                toastr.success('文件夹【' + self.folder + '】已添加！');
                            }
                            $uibModalInstance.close({
                                name: (self.type == 'file' ? self.file + self.extname : self.folder),
                                leaf: (self.type == 'file'),
                                parent: (self.parent.root ? '' : self.parent)
                            });
                        } else {
                            if (self.type == 'file') {
                                xDialog.alert('提示信息', '您创建的文件已存在！');
                            } else {
                                xDialog.alert('提示信息', '您创建的文件夹已存在！');
                            }
                        }
                    });
                } else {
                    var newNodeName = (self.type == 'file' ? self.file + self.extname : self.folder);
                    $http.post('/mock/data/rename', {
                        module: self.module,
                        oldPath: self.parent + '/' + nodeName,
                        newPath: self.parent + '/' + newNodeName
                    }).then(function(response) {
                        if(response.data) {
                            if (self.type == 'file') {
                                toastr.success('文件【' + nodeName + '】已修改！');
                            } else {
                                toastr.success('文件夹【' + nodeName + '】已修改！');
                            }
                            xContext.treeNode.name = newNodeName;
                            $uibModalInstance.close(true);
                        } else {
                            if (self.type == 'file') {
                                xDialog.alert('提示信息', '您重命名的文件已存在！');
                            } else {
                                xDialog.alert('提示信息', '您重命名的文件夹已存在！');
                            }
                        }
                    });
                }
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
