define(['app'], function(app) {
    app.controller('MarkdownCodeController', function($scope, xContext, $timeout, $uibModalInstance) {
        var self = this;

        self.languages = [
            { value: 'bash', label: 'Bash', mode: 'text' },
            { value: 'c#', label: 'C#', mode: 'csharp' },
            { value: 'c++', label: 'C++', mode: 'c_cpp' },
            { value: 'css', label: 'CSS', mode: 'css' },
            { value: 'html', label: 'HTML', mode: 'html' },
            { value: 'ini', label: 'Ini', mode: 'ini' },
            { value: 'json', label: 'JSON', mode: 'json' },
            { value: 'java', label: 'Java', mode: 'java' },
            { value: 'javascript', label: 'JavaScript', mode: 'javascript' },
            { value: 'markdown', label: 'Markdown', mode: 'markdown' },
            { value: 'nginx', label: 'Nginx', mode: 'text' },
            { value: 'objective-c', label: 'Objective-C', mode: 'objectivec' },
            { value: 'php', label: 'PHP', mode: 'php' },
            { value: 'properties', label: 'Properties', mode: 'properties' },
            { value: 'python', label: 'Python', mode: 'python' },
            { value: 'ruby', label: 'Ruby', mode: 'ruby' },
            { value: 'sql', label: 'SQL', mode: 'sql' },
            { value: 'shell', label: 'Shell', mode: 'sh' },
            { value: 'xml', label: 'XML', mode: 'xml' },
            { value: 'yaml', label: 'YAML', mode: 'yaml' }
        ];
        self.language = 'java';
        self.customized = false;

        var editor = null;

        self.init = function() {
            // code editor
            editor = ace.edit('code-editor');
            editor.setOptions({
                mode: 'ace/mode/text',
                fontSize: 13,
                useWorker: false,
                maxPixelHeight: 360,
                showPrintMargin: false,
                autoScrollEditorIntoView: true,
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true
            });

            $scope.$watchGroup(['dvm.customized', 'dvm.language'], function(newValue) {
                var modeId = 'ace/mode/text';
                if (!newValue[0]) {
                    modeId = 'ace/mode/' + _.find(self.languages, function(language) {
                        return language.value == newValue[1];
                    }).mode;
                }
                if (modeId != editor.getOption('mode')) {
                    editor.getSession().setMode(modeId);
                }
            });

            // load code
            if (!/^\s*$/.test(xContext.text)) {
                var rcode = /```\s*(\w+)\s*\n+(((?!```)[^])+)\n+```/g;
                var match = rcode.exec(xContext.text);
                if (match && match.length >= 3) {
                    self.language = match[1];
                    if (_.find(self.languages, function(language) {
                        return language.value == self.language;
                    })) {
                        self.customized = false;
                    } else {
                        self.customized = true;
                    }
                    $timeout(function() {
                        editor.setValue(match[2], -1);
                        editor.resize();
                    });
                }
            }
        }

        self.insert = function(valid) {
            if (valid) {
                $uibModalInstance.close({
                    language: self.language,
                    code: editor.getValue().trim()
                });
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
