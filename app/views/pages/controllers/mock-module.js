define(['app'], function(app) {
    app.controller('MockModuleController', function($http, xDialog, toastr) {
        var self = this;
        self.modules = [];

        self.init = function() {
            $http.post('/mock/module/get').then(function(response) {
                self.modules = response.data;
            });
        }

        self.edit = function(index) {
            var module = '';
            if (index >= 0) {
                module = self.modules[index];
            }
            xDialog.open().editModule(module).then(function(success) {
                if (success) {
                    self.init();
                }
            });
        }

        self.remove = function(index) {
            var module = self.modules[index];
            xDialog.confirm('确认信息', '您确定要删除【' + module + '】模块吗？').then(function(yes) {
                if (yes) {
                    $http.post('/mock/module/delete', { module: module }).then(function(response) {
                        if (response.data) {
                            toastr.success('数据模块【' + module + '】已删除！');
                            self.init();
                        }
                    });
                }
            });
        }
    });
});
