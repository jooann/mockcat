define(['app', 'clipboard'], function(app, Clipboard) {
    app.controller('CodeTemplaterController', function(toastr) {
        var self = this;

        self.parameters = {
            packageName: 'app.auth.bean',
            className: 'User',
            fields: [{
                type: 'String',
                name: 'username'
            }, {
                type: 'String',
                name: 'password'
            }]
        };
        self.template = [
            '<%',
            'var toUpperCase = function(name) {',
            '    return name.substring(0,1).toUpperCase() + name.substring(1);',
            '}',
            '%>',
            'package <%=packageName%>;',
            '',
            'import java.io.Serializable;',
            '',
            'public class <%=className%> implements Serializable {',
            '<%_.each(fields, function(field) {%>',
            '    private <%=field.type%> <%=field.name%>;',
            '<%});%><%_.each(fields, function(field) {%>',
            '    public <%=field.type%> get<%=toUpperCase(field.name)%>() {',
            '        return <%=field.name%>;',
            '    }',
            '',
            '    public void set<%=toUpperCase(field.name)%>(<%=field.type%> <%=field.name%>) {',
            '        this.<%=field.name%> = <%=field.name%>;',
            '    }',
            '<%});%>',
            '}'
            ].join('\n');
        self.code = '';

        self.init = function() {
            new Clipboard('#copy').on('success', function(e) {
                toastr.success('已复制到剪贴板！');
            });
        }

        self.reset = function() {
            self.parameters = {};
            self.code = '';
            self.template = '';
        }

        self.generate = function(valid) {
            if (valid) {
                self.code = _.template(self.template)(self.parameters);
            }
        }
    });
});
