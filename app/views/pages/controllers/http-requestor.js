define([
    'app',
    'clipboard',
    'json5',
    'jquery.form',
    'jquery.format',
    'ace-language-tools'
], function(app, Clipboard, JSON5) {
    app.controller('HttpRequestorController', function($scope, $http, $timeout, xDialog, xUtil, xLocation, toastr,
            $cookies, $routeParams, cfpLoadingBar) {
        var self = this;
        self.url = '';
        self.method = 'GET';
        self.cookies = '';
        self.sendType = 'send';
        self.completed = true;
        self.activeTab = 0;

        self.dataType = '';
        self.dataTypes = ['form-data', 'x-www-form-urlencoded', 'raw', 'binary'];

        self.rawDataType = {
            mode: 'text', value: '', label: 'Text'
        };
        self.rawDataTypes = [{
            mode: 'text', value: '', label: 'Text'
        }, {
            mode: 'text', value: 'text/plain', label: 'Text(text/plain)'
        }, {
            mode: 'hjson', value: 'application/json', label: 'JSON(application/json)'
        }, {
            mode: 'javascript', value: 'application/javascript', label: 'JavaScript(application/javascript)'
        }, {
            mode: 'xml', value: 'application/xml', label: 'XML(application/xml)'
        }, {
            mode: 'xml', value: 'text/xml', label: 'XML(text/xml)'
        }, {
            mode: 'html', value: 'text/html', label: 'HTML(text/html)'
        }];

        self.urlParsable = false;
        // parameters
        self.urlParams = [];
        self.headerParams = [];
        self.formParams = [];
        self.formDataParams = [];
        // response
        self.respActiveTab = 0;
        self.respStatus = 0;
        self.respBody = null;
        self.respHeaders = [];
        self.respCookies = [];

        var dataEditor = null, dataViewer = null;

        var dataParsable = true;
        self.dataParsable = true;

        var contentType = { name: 'Content-Type', checked: true, disabled: true };

        self.init = function() {
            // initial
            $timeout(function() {
                // data editor
                dataEditor = ace.edit('data-editor');
                dataEditor.setOptions({
                    mode: 'ace/mode/text',
                    fontSize: 13,
                    showPrintMargin: false,
                    autoScrollEditorIntoView: true,
                    enableBasicAutocompletion: true,
                    enableSnippets: true,
                    enableLiveAutocompletion: true
                });
                dataEditor.getSession().on('changeAnnotation', function(e) {
                    // check errors
                    $timeout(function() {
                        var annotations = dataEditor.getSession().getAnnotations();
                        var dataParsable = true;
                        if (!_.isEmpty(annotations)) {
                            if (_.find(annotations, function(annotation) {
                                return annotation.type == 'error';
                            })) {
                                dataParsable = false;
                            }
                        }
                        if (self.dataParsable != dataParsable) {
                            self.dataParsable = dataParsable;
                        }
                    });
                });

                // data viewer
                dataViewer = ace.edit('data-viewer');
                dataViewer.setOptions({
                    mode: 'ace/mode/text',
                    fontSize: 13,
                    readOnly: true,
                    useWorker: false,
                    showPrintMargin: false,
                    highlightActiveLine: false,
                    autoScrollEditorIntoView: true
                });

                new Clipboard('#copy', {
                    text: function() {
                        return dataViewer.getValue();
                    }
                }).on('success', function(e) {
                    toastr.success('已复制到剪贴板！');
                });

                $scope.$watch('vm.rawDataType', function(newValue) {
                    if (newValue) {
                        var modeId = 'ace/mode/' + newValue.mode;
                        if (modeId != dataEditor.getOption('mode')) {
                            dataEditor.getSession().setMode(modeId);
                        }
                    }
                });

                $scope.$watch('vm.respBody', function(newValue) {
                    if (newValue == null || newValue.data == null) {
                        dataViewer.setValue('', -1);
                    } else {
                        var modeId = 'ace/mode/' + newValue.mode;
                        if (modeId != dataViewer.getOption('mode')) {
                            dataViewer.getSession().setMode(modeId);
                        }
                        dataViewer.setValue(newValue.data, -1);
                    }
                    dataViewer.resize();
                });

                if ($routeParams.target) {
                    loadTestTarget();
                } else {
                    loadSample();
                }
            });

            $scope.$watch('vm.url', function(newValue) {
                self.urlParsable = /http(s)?:\/\/[\w-\.]+(:\d+)?(\/[\w-./?%&=]*)?(?:#(.*))?/.test(newValue);
            });

            $scope.$watchGroup(['vm.dataType', 'vm.rawDataType'], function(newValue) {
                switch(newValue[0]) {
                    case 'binary':
                    case 'form-data':
                        contentType.value = 'multipart/form-data';
                        break;
                    case 'x-www-form-urlencoded':
                        contentType.value = 'application/x-www-form-urlencoded';
                        break;
                    case 'raw':
                        contentType.value = newValue[1].value;
                        break;
                    default:
                        contentType.value = '';
                }
                var index = self.headerParams.indexOf(contentType);
                if (contentType.value == '') {
                    if (index != -1) {
                        self.headerParams.splice(index, 1);
                    }
                } else if (index == -1) {
                    self.headerParams.unshift(contentType);
                }
            });
        }

        function loadSample() {
            self.url = 'http://127.0.0.1:8080/app/auth/login';
            self.method = 'POST';
            self.activeTab = 2;
            self.dataType = 'raw';
            self.rawDataType = {
                mode: 'hjson', value: 'application/json', label: 'JSON(application/json)'
            };
            dataEditor.setValue([
                '{',
                '    "username": "admin",',
                '    "password": "123456"',
                '}'].join('\n'), -1);
            dataEditor.resize();
        }

        function loadTestTarget() {
            $http({
                method: 'POST',
                url: '/' + $routeParams.target + '?origin=true',
                transformResponse: function(response) {
                    return response;
                }
            }).then(function(response) {
                var path = xUtil.reg.subMatch(response.data, /URL：(.*)/gi);
                var host = $cookies.get('test_host');
                if (_.isEmpty(host)) {
                    host = 'http://127.0.0.1:8080';
                }
                self.url = host + path;

                self.method = xUtil.reg.subMatch(response.data, /Method：(get|post)/gi).toUpperCase();
                self.method = (self.method == '' ? 'GET' : self.method);

                try {
                    var headers = _.map(JSON5.parse(xUtil.reg.subMatch(response.data, /Headers：([\s\S]*)入参：/gm) || '{}'),
                            function(value, name) {
                                return {
                                    name: name,
                                    value: value,
                                    checked: true,
                                    disabled: false
                                };
                            });
                    headers.push({
                        name: '',
                        value: '',
                        checked: true,
                        disabled: false
                    });
                    self.headerParams = headers;
                } catch (e) {
                    toastr.error('解析【Headers】错误：' + e);
                }

                var requestBody = _.compose(function(match) {
                    return match ? match : xUtil.reg.subMatch(response.data, /入参：([\s\S]*)出参：/gm);
                }, function() {
                    return xUtil.reg.subMatch(response.data, /入参：([\s\S]*)错误：/gm);
                })().trim();

                if (!_.isEmpty(requestBody)) {
                    $timeout(function() {
                        self.activeTab = 2;
                        self.dataType = 'raw';
                        var extname = $routeParams.target.substring($routeParams.target.lastIndexOf('.'));
                        if (extname == '.json') {
                            self.rawDataType = {
                                mode: 'hjson', value: 'application/json', label: 'JSON(application/json)'
                            };
                        } else if (extname == '.xml') {
                            self.rawDataType = {
                                mode: 'xml', value: 'application/xml', label: 'XML(application/xml)'
                            };
                        }
                        dataEditor.setValue(requestBody, -1);
                        dataEditor.resize();
                    });
                }
            });
        }

        self.callback = function(height, width) {
            // size of request editor
            jQuery('.x-request .tab-pane').css('height', (height - 106) + 'px');
            jQuery('.x-request .tab-pane .x-editor pre').css('min-height', (height - 143) + 'px');
            // size of response editor
            jQuery('.x-response .tab-pane').css('height', (height - 57) + 'px');
            jQuery('.x-response .tab-pane pre').css('height', (height - 57) + 'px');
        }

        self.reset = function() {
            self.url = '';
            self.method = 'GET';
            self.cookies = '';
            self.completed = true;
            self.activeTab = 0;

            self.dataType = '';
            self.rawDataType = {
                mode: 'text', value: '', label: 'Text'
            };

            self.urlParsable = false;
            // parameters
            self.urlParams = [];
            self.headerParams = [{
                name: '',
                value: '',
                checked: true,
                disabled: false
            }];
            self.formParams = [{
                name: '',
                value: '',
                checked: true,
                disabled: false
            }];
            self.formDataParams = [{
                name: '',
                type: 'text',
                value: '',
                checked: true,
                disabled: false
            }];
            // response
            self.respActiveTab = 0;
            self.respStatus = 0;
            self.respBody = null;
            self.respHeaders = [];
            self.respCookies = [];

            dataEditor.setValue('');
            self.dataParsable = true;
        }

        self.editParams = function() {
            var location = xLocation.parse(self.url);
            self.urlParams = _.map(location.search(), function(value, name) {
                return { name: name, value: value };
            });
            xDialog.open().editParams({ params: self.urlParams }).then(function() {
                var params = {};
                _.forEach(self.urlParams, function(param) {
                    if (param.name != '') {
                        params[param.name] = param.value;
                    }
                });
                location.search(params);
                self.url = location.url();
            });
        }

        self.changeMethod = function(method) {
            if (method == 'GET' || method == 'HEAD') {
                self.activeTab = 0;
                self.dataParsable = true;
            } else {
                self.dataParsable = dataParsable;
            }
        }

        self.sendRequest = function(valid) {
            self.respActiveTab = 0;
            self.respStatus = 0;
            self.respBody = null;
            self.respHeaders = [];
            self.respCookies = [];
            // send request
            if (valid) {
                self.completed = false;

                if ($routeParams.target) {
                    var matches = self.url.match(/^http(s)?:\/\/[\w-\.]+(:\d+)?/);
                    if (!_.isEmpty(matches)) {
                        $cookies.put('test_host', matches[0]);
                    }
                }

                // create form
                var $form = jQuery('<form style="display:none">').attr({
                    'accept-charset': 'UTF-8',
                    'enctype': 'multipart/form-data'
                });

                // hidden parameters
                _.forEach([
                    { name: '__url', value: self.url },
                    { name: '__method', value: self.method },
                    { name: '__headers', value: buildHeaders() },
                    { name: '__cookies', value: buildCookies() }
                ], function(param) {
                    $form.append(jQuery('<input type="hidden">').attr({
                        name: param.name,
                        value: param.value
                    }));
                });

                // body
                if (self.method != 'GET' && self.method != 'HEAD') {
                    switch(self.dataType) {
                        case 'form-data':
                        case 'x-www-form-urlencoded':
                            var fields = {};
                            var params = (self.dataType == 'form-data' ? self.formDataParams : self.formParams);
                            _.forEach(params, function(param) {
                                if (param.checked && param.name != '') {
                                    if (param.type == 'file') {
                                        $form.append(jQuery('#form-data input[name="' + param.name + '"]').clone());
                                    } else {
                                        fields[param.name] = param.value;
                                    }
                                }
                            });
                            $form.append(jQuery('<textarea name="__body">').val(JSON.stringify(fields)));
                            break;
                        case 'raw':
                            var body = dataEditor.getValue();
                            if (self.rawDataType.value == 'application/json' && body != '') {
                                body = JSON.stringify(JSON5.parse(body));
                            }
                            $form.append(jQuery('<textarea name="__body">').val(body));
                            break;
                        case 'binary':
                            $form.append(jQuery('#binary input[name="file"]').clone());
                            break;
                        default: break;
                    }
                }
                $form.appendTo(document.body);

                // send request
                $form.ajaxSubmit({
                    url: '/mock/test/request',
                    method: 'POST',
                    headers: {
                        'Pragma': 'no-cache',
                        'Cache-Control': 'no-cache'
                    },
                    iframe: (self.sendType == 'download'),
                    iframeSrc: '',
                    timeout: 65000,
                    dataType: 'text',
                    beforeSend: function() {
                        cfpLoadingBar.start();
                        return true;
                    },
                    complete: function(jqXHR, textStatus) {
                        self.respStatus = jqXHR.status;
                        self.respHeaders = parseHeaders(jqXHR.getAllResponseHeaders());
                        self.respCookies = parseCookies(jqXHR.getResponseHeader('x-cookies'));

                        if (jqXHR.status != 200 && _.isEmpty(jqXHR.responseText)) {
                            var message = (self.sendType == 'download' ? textStatus : jqXHR.statusText);
                            xDialog.alert('错误信息', '服务器的返回信息：' + message);
                        } else {
                            var contentType = jqXHR.getResponseHeader('Content-Type');
                            if (/\/json/.test(contentType)) {
                                self.respBody = {
                                    mode: 'json',
                                    data: jQuery.format(jqXHR.responseText, { method: 'json' })
                                };
                            } else if (/\/xml/.test(contentType)) {
                                self.respBody = {
                                    mode: 'xml',
                                    data: jQuery.format(jqXHR.responseText, { method: 'xml' })
                                };
                            } else if (/\/javascript/.test(contentType)) {
                                self.respBody = { mode: 'javascript', data: jqXHR.responseText };
                            } else if (/\/html/.test(contentType)) {
                                self.respBody = { mode: 'html', data: jqXHR.responseText };
                            } else {
                                self.respBody = { mode: 'text', data: jqXHR.responseText };
                            }
                        }

                        $form.remove();
                        self.completed = true;
                        cfpLoadingBar.complete();
                        $timeout(function() { $scope.$apply(); });
                    }
                });

                // release after 60s when download
                if (self.sendType == 'download') {
                    $timeout(function() {
                        if (!self.completed) {
                            $form.remove();
                            self.completed = true;
                            cfpLoadingBar.complete();
                        }
                    }, 60000);
                }

                toastr.success('HTTP请求已发送！');
            }
        }

        function buildHeaders() {
            var headers = {};
            _.forEach(self.headerParams, function(param) {
                if (param.checked && param.name != '') {
                    headers[param.name] = param.value;
                }
            });
            return JSON.stringify(headers);
        }

        function buildCookies() {
            var cookies = {};
            if (self.cookies) {
                var cookie = self.cookies.trim().replace(/\n/g, ';'),
                    pattern = /([^=]+)=([^;]+);?\s*/g,
                    match = null;
                while ((match = pattern.exec(cookie))) {
                    cookies[match[1]] = match[2];
                }
            }
            return JSON.stringify(cookies);
        }

        function parseHeaders(headersString) {
            var headers = {};
            if (headersString) {
                var rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
                    match = null;
                while ((match = rheaders.exec(headersString))) {
                    if (match[1] != 'x-cookies') {
                        headers[match[1]] = match[2];
                    }
                }
            }
            return _.map(headers, function(value, name) {
                return { name: name, value: value };
            });
        }

        function parseCookies(cookies) {
            return (cookies || '').split('@');
        }
    });
});
