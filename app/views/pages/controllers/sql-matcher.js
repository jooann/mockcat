define(['app', 'clipboard', 'sql-formatter'], function(app, Clipboard, sqlFormatter) {
    app.controller('SqlMatcherController', function($scope, toastr) {
        var self = this;
        self.preparedSql = '';
        self.parameters = '';
        self.code = '';

        const quotTypes = ['String', 'Date', 'Timestamp'];
        var sqlViewer = null;

        self.init = function() {
            self.preparedSql = 'SELECT * FROM users WHERE username = ? AND password = ?';
            self.parameters = 'admin(String), 123456(String)';

            new Clipboard('#copy', {
                text: function() {
                    return self.code;
                }
            }).on('success', function(e) {
                toastr.success('已复制到剪贴板！');
            });

            // sql viewer
            sqlViewer = ace.edit('sql-viewer');
            sqlViewer.setOptions({
                mode: 'ace/mode/sql',
                fontSize: 13,
                readOnly: true,
                useWorker: false,
                showPrintMargin: false,
                highlightActiveLine: false,
                autoScrollEditorIntoView: true
            });

            $scope.$watch('vm.code', function(newValue) {
                sqlViewer.setValue(newValue, -1);
                sqlViewer.resize();
            });
        }

        self.reset = function() {
            self.preparedSql = '';
            self.parameters = '';
            self.code = '';
        }

        self.match = function(valid) {
            if (valid) {
                var parameters = [];
                angular.forEach(self.parameters.split(','), function(parameter) {
                    parameter = parameter.trim();
                    var value = parameter.replace(/\(.*\)/, '');
                    var type = parameter.replace(/\(|\)/g, '').substring(value.length);
                    if (quotTypes.indexOf(type) >= 0) {
                        value = '\'' + value + '\'';
                    }
                    parameters.push(value);
                });

                self.code = sqlFormatter.format(self.preparedSql, {
                    indent: '    ',
                    language: 'sql',
                    params: parameters
                });
            }
        }
    });
});
