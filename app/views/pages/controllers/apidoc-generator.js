define(['app', 'clipboard'], function(app, Clipboard) {
    app.controller('ApiDocGeneratorController', function(toastr) {
        var self = this;

        self.code = '';
        var repeatNodes = [{
            name: 'apiParam',
            times: 3
        }, {
            name: 'apiSuccess',
            times: 3
        }];

        self.init = function() {
            new Clipboard('#copy').on('success', function(e) {
                toastr.success('已复制到剪贴板！');
            });
            _.forEach(self.apiParams, function(apidoc) {
                _.forEach(repeatNodes, function(node) {
                    if (apidoc.name == node.name) {
                        apidoc['multiple'] = true;
                        _.forEach(_.range(node.times - 1), function() {
                            apidoc.rows.push(angular.copy(apidoc.rows[0]));
                        });
                    }
                });
            });
        }

        var isEmpty = function(value) {
            return value.replace(/^s+|s+$/g, '') === '';
        }

        self.generate = function(valid) {
            self.code = '';
            if (valid) {
                var code = '/**';
                _.forEach(self.apiParams, function(apidoc) {
                    if (apidoc.checked) {
                        var params = '', values = '';
                        if (!apidoc['multiple']) {
                            params = '\n * @' + apidoc.name;
                        }

                        _.forEach(apidoc.rows, function(row) {
                            if (apidoc['multiple']) {
                                params = '\n * @' + apidoc.name;
                                values = '';
                            }
                            _.forEach(row, function(item) {
                                if (item.value) {
                                    var value = '';
                                    if (item.format) {
                                        value = item.format.replace('s', item.value);
                                    } else {
                                        value = item.value;
                                    }
                                    if (item.name == 'example') {
                                        value = ('\n' + value).replace(/\n/g, '\n *   ');
                                    } else {
                                        value = ' ' + value;
                                    }
                                    values += value
                                }
                            });
                            if (apidoc['multiple'] && !isEmpty(values)) {
                                code += params + values;
                            }
                        });
                        if (!apidoc['multiple'] && !isEmpty(values)) {
                            code += params + values;
                        }
                        if (_.size(apidoc.rows) == 0) {
                            code += params;
                        }
                    }
                });
                code += '\n */';

                self.code = code;
            }
        }

        self.reset = function() {
            self.code = '';
            _.forEach(self.apiParams, function(apidoc) {
                apidoc.checked = true;
                _.forEach(apidoc.rows, function(row) {
                    _.forEach(row, function(item) {
                        if (item.type == 'select') {
                            item.value = item.options[0].value;
                        } else {
                            item.value = '';
                        }
                    });
                });
            });
        }

        self.apiParams = [{
            name: 'api',
            checked: true,
            rows: [[{
                name: 'method',
                value: 'post',
                format: '{s}',
                type: 'select',
                width: 3,
                options: [{
                    value: 'get',
                    label: 'GET'
                }, {
                    value: 'post',
                    label: 'POST'
                }]
            }, {
                name: 'path',
                value: '/app/auth/login',
                type: 'text',
                width: 6,
                placeholder: 'path'
            }, {
                name: 'title',
                value: '用户登录',
                type: 'text',
                width: 3,
                placeholder: '[title]'
            }]]
        }, {
            name: 'apiDefine',
            checked: true,
            rows: [[{
                name: 'name',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'name'
            }, {
                name: 'title',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[title]'
            }, {
                name: 'description',
                value: '',
                type: 'text',
                width: 6,
                placeholder: '[description]'
            }]]
        }, {
            name: 'apiDeprecated',
            checked: true,
            rows: [[{
                name: 'text',
                value: '',
                type: 'text',
                width: 6,
                placeholder: '[text]'
            }]]
        }, {
            name: 'apiDescription',
            checked: true,
            rows: [[{
                name: 'text',
                value: '',
                type: 'text',
                width: 6,
                placeholder: 'text'
            }]]
        }, {
            name: 'apiError',
            checked: true,
            rows: [[{
                name: 'group',
                value: '',
                format: '(s)',
                type: 'text',
                width: 3,
                placeholder: '[(group)]'
            }, {
                name: 'type',
                value: '',
                format: '(s)',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'field',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'field'
            }, {
                name: 'description',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[description]'
            }]]
        }, {
            name: 'apiErrorExample',
            checked: true,
            rows: [[{
                name: 'type',
                value: '',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'title',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[title]'
            }], [{
                name: 'example',
                value: '',
                type: 'textarea',
                width: 12,
                placeholder: 'example'
            }]]
        }, {
            name: 'apiExample',
            checked: true,
            rows: [[{
                name: 'type',
                value: '',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'title',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'title'
            }], [{
                name: 'example',
                value: '',
                type: 'textarea',
                width: 12,
                placeholder: 'example'
            }]]
        }, {
            name: 'apiGroup',
            checked: true,
            rows: [[{
                name: 'name',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'name'
            }]]
        }, {
            name: 'apiHeader',
            checked: true,
            rows: [[{
                name: 'group',
                value: '',
                format: '(s)',
                type: 'text',
                width: 3,
                placeholder: '[(group)]'
            }, {
                name: 'type',
                value: '',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'field',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[field]'
            }, {
                name: 'description',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[description]'
            }]]
        }, {
            name: 'apiHeaderExample',
            checked: true,
            rows: [[{
                name: 'type',
                value: '',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'title',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[title]'
            }], [{
                name: 'example',
                value: '',
                type: 'textarea',
                width: 12,
                placeholder: 'example'
            }]]
        }, {
            name: 'apiIgnore',
            checked: true,
            rows: [[{
                name: 'hint',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[hint]'
            }]]
        }, {
            name: 'apiName',
            checked: true,
            rows: [[{
                name: 'name',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'name'
            }]]
        }, {
            name: 'apiParam',
            checked: true,
            rows: [[{
                name: 'group',
                value: '',
                format: '(s)',
                type: 'text',
                width: 3,
                placeholder: '[(group)]'
            }, {
                name: 'type',
                value: '',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'field',
                value: '',
                format: '[s]',
                type: 'text',
                width: 3,
                placeholder: '[field]'
            }, {
                name: 'description',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[description]'
            }]]
        }, {
            name: 'apiParamExample',
            checked: true,
            rows: [[{
                name: 'type',
                value: 'json',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'title',
                value: '入参',
                type: 'text',
                width: 3,
                placeholder: '[title]'
            }], [{
                name: 'example',
                value: '{\n    "username": "admin",\n    "password": "123456"\n}',
                type: 'textarea',
                width: 12,
                placeholder: 'example'
            }]]
        }, {
            name: 'apiPermission',
            checked: true,
            rows: [[{
                name: 'name',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'name'
            }]]
        }, {
            name: 'apiPrivate',
            checked: false,
            rows: []
        }, {
            name: 'apiSampleRequest',
            checked: true,
            rows: [[{
                name: 'url',
                value: '',
                type: 'text',
                width: 6,
                placeholder: 'url'
            }]]
        }, {
            name: 'apiSuccess',
            checked: true,
            rows: [[{
                name: 'group',
                value: '',
                format: '(s)',
                type: 'text',
                width: 3,
                placeholder: '[(group)]'
            }, {
                name: 'type',
                value: '',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'field',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'field'
            }, {
                name: 'description',
                value: '',
                type: 'text',
                width: 3,
                placeholder: '[description]'
            }]]
        }, {
            name: 'apiSuccessExample',
            checked: true,
            rows: [[{
                name: 'type',
                value: 'json',
                format: '{s}',
                type: 'text',
                width: 3,
                placeholder: '[{type}]'
            }, {
                name: 'title',
                value: '出参',
                type: 'text',
                width: 3,
                placeholder: '[title]'
            }], [{
                name: 'example',
                value: '{\n    "success": true,\n    "redirect": "/index.html"\n}',
                type: 'textarea',
                width: 12,
                placeholder: 'example'
            }]]
        }, {
            name: 'apiUse',
            checked: true,
            rows: [[{
                name: 'name',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'name'
            }]]
        }, {
            name: 'apiVersion',
            checked: true,
            rows: [[{
                name: 'version',
                value: '',
                type: 'text',
                width: 3,
                placeholder: 'version'
            }]]
        }];
    });
});
